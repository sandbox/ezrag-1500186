api = 2
core = 7.x
includes[] = drupal-org-core.make

projects[panopoly][download][type] = git
projects[panopoly][download][url] = "ezrag@git.drupal.org:sandbox/ezrag/1500186.git"
projects[panopoly][download][branch] = "7.x-1.x"
